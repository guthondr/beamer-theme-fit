# Barevné schéma ČVUT FIT pro LaTeX Beamer

Toto schéma (theme) a barevné schéma (color theme) není standardní součástí LaTeX Beamer
a ani není oficiálním schématem Fakulty informačních technologií ČVUT v Praze.

## Použití

Pro správné použití jsou třeba loga ČVUT dostupná z https://cvut.cz konkrétně soubory
logo_FIT.pdf, logo_FIT_negativ.pdf a symbol_cvut_konturova_verze_cb.pdf

```latex
\pgfdeclareimage[height=0.8cm]{logocvut}{logo_FIT}
\pgfdeclareimage[height=0.8cm]{logocvutneg}{logo_FIT_negativ}
\pgfdeclareimage[width=.7\textwidth]{symbolcvut}{symbol_cvut_konturova_verze_cb}

\usetheme{CVUTFIT}
```

Pro použití písma Technika (dostupné z https://cvut.cz) lze použít balíček fontspec a příkaz
```latex
\setmainfont{Technika-Light}
```
